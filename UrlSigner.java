import java.io.UnsupportedEncodingException;
import java.lang.Exception;
import java.lang.String;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.LinkedHashMap;
import java.util.Map;

public class UrlSigner {

    public static void main(String[] args) throws Exception {
        UrlSigner urlSigner = new UrlSigner("mysecret");

        URI uri = urlSigner.sign("https://photos.cern.ch/?ais_id=343434", 10);
        System.out.println(uri);
        System.out.println(urlSigner.validate(uri));
        uri = urlSigner.sign("https://photos.cern.ch/?ais_id=343434&debug", 10);
        System.out.println(uri);
        System.out.println(urlSigner.validate(uri));
        uri = urlSigner.sign("https://photos.cern.ch/?ais_id=343434&debug", 0);
        System.out.println(uri);
        System.out.println(urlSigner.validate(uri));
    }

    public class InvalidExpiration extends Exception {
        public InvalidExpiration(String msg) {
            super(msg);
        }
    }

    private String signatureKey;
    private String expiresParameter;
    private String signatureParameter;
    private String debugParameter;
    private String timeParameter;

    public UrlSigner(String signatureKey) {
        this(signatureKey, "expires");
    }

    public UrlSigner(String signatureKey, String expiresParameter) {
        this(signatureKey, expiresParameter, "signature");
    }

    public UrlSigner(String signatureKey, String expiresParameter, String signatureParameter) {
        this.signatureKey = signatureKey;
        this.expiresParameter = expiresParameter;
        this.signatureParameter = signatureParameter;
        this.debugParameter = "debug";
        this.timeParameter = "time";
    }

    public URI sign(String uri, Object days_or_expiration) throws InvalidExpiration, UnsupportedEncodingException, URISyntaxException {
        return sign(new URI(uri), days_or_expiration);
    }

    public URI sign(URI uri, Object days_or_expiration) throws InvalidExpiration, UnsupportedEncodingException, URISyntaxException {
        long expiration = getExpirationTimestamp(days_or_expiration);

        String signature = createSignature(uri, expiration);

        return signUrl(uri, expiration, signature);
    }

    public boolean validate(String uri) throws UnsupportedEncodingException, URISyntaxException {
        return validate(new URI(uri));
    }

    public boolean validate(URI uri) throws UnsupportedEncodingException, URISyntaxException {
        Map<String, String> query = splitQuery(uri);

        if (isMissingAQueryParameter(query)) {
            return false;
        }

        LocalDateTime expiration = toLocalDateTime(query.get(expiresParameter));

        if (! isFuture(expiration)) {
            return false;
        }

        if (! hasValidSignature(uri)) {
            return false;
        }

        return true;
    }

    protected boolean isMissingAQueryParameter(Map<String, String> query) {
        if (query.get(expiresParameter) == null) {
            return true;
        }

        if (query.get(signatureParameter) == null) {
            return true;
        }

        return false;
    }

    protected boolean isFuture(LocalDateTime timestamp) {
        return timestamp.isAfter(LocalDateTime.now());
    }

    protected URI getIntendedUrl(URI uri) throws UnsupportedEncodingException, URISyntaxException {
        Map<String, String> intendedQuery = splitQuery(uri);

        intendedQuery.remove(expiresParameter);
        intendedQuery.remove(signatureParameter);

        return appendQueryToUrl(uri, intendedQuery);
    }

    protected long getExpirationTimestamp(Object days_or_expiration) throws InvalidExpiration {
        LocalDateTime expiration;
        if (days_or_expiration instanceof Integer) {
            int days = (int)days_or_expiration;
            if (days == 0) {
                expiration = LocalDateTime.of(2100, 1, 1, 0, 0, 0);
            } else {
                expiration = LocalDateTime.now().plusDays(days);
            }
        } else if (days_or_expiration instanceof LocalDateTime) {
            expiration = (LocalDateTime)days_or_expiration;
        } else {
            throw new InvalidExpiration("Expiration date must be an instance of LocalDateTime or an integer");
        }

        if (! isFuture(expiration)) {
            throw new InvalidExpiration("Expiration date must be in the future");
        }

        return expiration.atZone(ZoneId.systemDefault()).toEpochSecond();
    }

    private LocalDateTime toLocalDateTime(String timeInSeconds) {
        return toLocalDateTime(Long.parseLong(timeInSeconds));
    }

    private LocalDateTime toLocalDateTime(long timeInSeconds) {
        return LocalDateTime.ofInstant(Instant.ofEpochSecond(timeInSeconds), ZoneId.systemDefault());
    }

    protected boolean hasValidSignature(URI uri) throws UnsupportedEncodingException, URISyntaxException {
        Map<String, String> query = splitQuery(uri);

        long expiration = Long.parseLong(query.get(expiresParameter));
        String providedSignature = query.get(signatureParameter);

        URI intendedUri = getIntendedUrl(uri);

        String validSignature = createSignature(intendedUri, expiration);

        return validSignature.equals(providedSignature);
    }

    protected String createSignature(URI uri, long expiration) throws UnsupportedEncodingException, URISyntaxException {
        Map<String, String> intendedQuery = splitQuery(uri);

        intendedQuery.remove(debugParameter);
        intendedQuery.remove(timeParameter);

        String url = appendQueryToUrl(uri, intendedQuery).getQuery().toString();

        // System.out.println(String.format("%s::%d::%s", url, expiration, signatureKey));
        return MD5(String.format("%s::%d::%s", url, expiration, signatureKey));
    }

    protected URI signUrl(URI uri, long expiration, String signature) throws UnsupportedEncodingException, URISyntaxException {
        Map<String, String> query = splitQuery(uri);

        query.put(expiresParameter, Long.toString(expiration));
        query.put(signatureParameter, signature);

        return appendQueryToUrl(uri, query);
    }

    private static Map<String, String> splitQuery(URI uri) throws UnsupportedEncodingException {
        Map<String, String> query_pairs = new LinkedHashMap<String, String>();
        String query = uri.getQuery();
        String[] pairs = query.split("&");
        for (String pair : pairs) {
            int idx = pair.indexOf("=");
            if (idx >= 0) {
                query_pairs.put(URLDecoder.decode(pair.substring(0, idx), "UTF-8"), URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
            } else {
                query_pairs.put(URLDecoder.decode(pair, "UTF-8"), null);
            }
        }
        return query_pairs;
    }

    private static URI appendQueryToUrl(URI uri, Map<String, String> parameters) throws UnsupportedEncodingException, URISyntaxException {
        // String query = uri.getQuery();

        StringBuilder builder = new StringBuilder();

        // if (query != null)
        //     builder.append(query);

        for (Map.Entry<String, String> entry: parameters.entrySet())
        {
            String keyValueParam = entry.getKey() + (entry.getValue() != null ? "=" + entry.getValue() : "");
            if (!builder.toString().isEmpty())
                builder.append("&");

            builder.append(keyValueParam);
        }

        return new URI(uri.getScheme(), uri.getAuthority(), uri.getPath(), builder.toString(), uri.getFragment());
    }

    private static String MD5(String md5) {
        try {
                MessageDigest md = MessageDigest.getInstance("MD5");
                byte[] array = md.digest(md5.getBytes());
                StringBuffer sb = new StringBuffer();
                for (int i = 0; i < array.length; ++i) {
                    sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1,3));
                }
                return sb.toString();
            } catch (NoSuchAlgorithmException e) {
        }
        return null;
    }
}
