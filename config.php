<?php

// if (!defined('DATATABLES')) exit(); // Ensure being used in DataTables env.

// Enable error reporting for debugging (remove for production)
error_reporting(E_ALL & ~E_STRICT);
ini_set('display_errors', '1');

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Database user / pass
 *
 * Example for Test Database, just needs password
 */
$sql_details = array(
	"type" => NULL,         // Database type: "Mysql", "Postgres", "Sqlserver", "Sqlite" or "Oracle"
	"user" => "",   		// Database user name
	"pass" => "",           // Database password (TBS)
	"host" => NULL, 		// Database host
	"port" => NULL,         // Database connection port (can be left empty for default)
	"db"   => NULL,         // Database name (dev: devdb19; test: int8r; prod: atlr)
	"dsn"  => "",           // PHP DSN extra information. Set as `charset=utf8mb4` if you are using MySQL
	"pdoAttr" => array()   	// PHP PDO attributes array. See the PHP documentation for all options
);

$photos_details = array(
    "url" => NULL,
    "login" => NULL,
    "password" => NULL,
    "secret" => NULL
);

$local_config = 'config-local.php';
$path = stream_resolve_include_path($local_config);
if ($path && file_exists($path)) {
    include($local_config);
}

$sql_details["type"] = getenv("ORACLE_DB_TYPE") ?: $sql_details["type"];
if (!$sql_details["type"]) {
    header("HTTP/1.0 502 Bad Gateway");
    die("ORACLE_DB_TYPE not defined\n");
}

$sql_details["user"] = getenv("ORACLE_DB_USER") ?: $sql_details["user"];
if (!$sql_details["user"]) {
    header("HTTP/1.0 502 Bad Gateway");
    die("ORACLE_DB_USER not defined\n");
}

$sql_details["pass"] = getenv("ORACLE_DB_PASSWORD") ?: $sql_details["pass"];
if (!$sql_details["pass"]) {
    header("HTTP/1.0 502 Bad Gateway");
    die("ORACLE_DB_PASSWORD not defined\n");
}

$sql_details["db"] = getenv("ORACLE_DB") ?: $sql_details["db"];
if (!$sql_details["db"]) {
    header("HTTP/1.0 502 Bad Gateway");
    die("ORACLE_DB not defined\n");
}


$photos_details["url"] = getenv("PHOTOS_URL") ?: $photos_details["url"];
if (!$photos_details["url"]) {
    header("HTTP/1.0 502 Bad Gateway");
    die("PHOTOS_URL not defined\n");
}

$photos_details["login"] = getenv("PHOTOS_LOGIN") ?: $photos_details["login"];
if (!$photos_details["login"]) {
    header("HTTP/1.0 502 Bad Gateway");
    die("PHOTOS_LOGIN not defined\n");
}

$photos_details["password"] = getenv("PHOTOS_PASSWORD") ?: $photos_details["password"];
if (!$photos_details["password"]) {
    header("HTTP/1.0 502 Bad Gateway");
    die("PHOTOS_PASSWORD not defined\n");
}

$photos_details["secret"] = getenv("PHOTOS_SECRET") ?: $photos_details["secret"];
if (!$photos_details["secret"]) {
    header("HTTP/1.0 502 Bad Gateway");
    die("PHOTOS_SECRET not defined\n");
}
