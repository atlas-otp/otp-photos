<?php

require_once('PhotoUrlSigner.php');

if (count($argv) < 3) {
    echo "Usage simple_signer_test.php secret ais_id\n";
    exit(1);
}

$secret = $argv[1];         # secret used to access the photos
$ais_id = $argv[2];         # ais_id of the person you need the photo from

$urlSigner = new PhotoUrlSigner($secret);
$signedUrl = $urlSigner->sign_id($ais_id);
print($signedUrl ."\n");

# just to check
print($urlSigner->validate($signedUrl) . "\n");
?>
