<?php

require_once('PhotoUrlSigner54.php');

$urlSigner = new PhotoUrlSigner('mysecret');
$url = $urlSigner->sign('https://photos.cern.ch/', 10);
print($url ."\n");
print($urlSigner->validate($url) . "\n");

$url = $urlSigner->sign('https://photos.cern.ch/?ais_id=343434&debug', 10);
print($url ."\n");
print($urlSigner->validate($url) . "\n");

$url = $urlSigner->sign('https://photos.cern.ch/?ais_id=343434', 10);
print($url ."\n");
print($urlSigner->validate($url . "&debug") . "\n");

$url = $urlSigner->sign('https://photos.cern.ch/?ais_id=343434&debug&output=hex', 10);
print($url ."\n");
print($urlSigner->validate($url . "&debug") . "\n");

$url = $urlSigner->sign('https://photos.cern.ch/?ais_id=343434&output=hex', 10);
print($url ."\n");
print($urlSigner->validate($url . "&debug") . "\n");

$url = $urlSigner->sign('https://photos.cern.ch/?ais_id=343434', 0);
print($url ."\n");
print($urlSigner->validate($url . "&debug") . "\n");

$url = $urlSigner->sign('https://www.cern.ch/?ais_id=343434', 0);
print($url ."\n");
print($urlSigner->validate($url . "&debug") . "\n");
